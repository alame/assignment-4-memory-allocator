#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
void debug(const char *fmt, ...);
struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
    void* allocated = mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
    if (allocated== MAP_FAILED) allocated = NULL;
    return allocated;
}
void test_malloc(){
    debug("test malloc\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    void* allocated = _malloc(2048);
    assert(allocated);
    allocated = _malloc(4096);
    assert(allocated);
    heap_term();
    debug("test passed\n");
}
void test_free_one_block(){
    debug("test free one block of three\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    void* first_block = _malloc(1024);
    void* second_block = _malloc(2048);
    void* third_block = _malloc(4096);
    assert(first_block);
    assert(second_block);
    assert(third_block);
    assert(!block_get_header(first_block)->is_free);
    assert(!block_get_header(second_block)->is_free);
    assert(!block_get_header(third_block)->is_free);
    _free(first_block);
    assert(block_get_header(first_block)->is_free);
    assert(!block_get_header(second_block)->is_free);
    assert(!block_get_header(third_block)->is_free);
    heap_term();
    debug("test passed\n");
}
void test_free_two_blocks(){
    debug("test free two block of three\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    void* first_block = _malloc(1024);
    void* second_block = _malloc(2048);
    void* third_block = _malloc(4096);
    assert(first_block);
    assert(second_block);
    assert(third_block);
    assert(!block_get_header(first_block)->is_free);
    assert(!block_get_header(second_block)->is_free);
    assert(!block_get_header(third_block)->is_free);
    _free(second_block);
    _free(third_block);
    assert(!block_get_header(first_block)->is_free);
    assert(block_get_header(second_block)->is_free);
    assert(block_get_header(third_block)->is_free);
    heap_term();
    debug("test passed\n");
}
void test_expand_heap(){
    debug("Test: grow heap\n");
    struct region *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    size_t start_size = heap->size;
    _malloc(REGION_MIN_SIZE*2);
    size_t new_size = heap->size;
    assert(new_size>start_size);
    heap_term();
    debug("test passed\n");
}
void test_new_region_in_another_place(){
    debug("test new region in another place\n");
    void *allocated = map_pages(HEAP_START, REGION_MIN_SIZE, MAP_FIXED);
    assert(allocated);
    void *new_allocated = _malloc(REGION_MIN_SIZE);
    assert(new_allocated);
    assert(allocated!=new_allocated);
    debug("test passed\n");
}
int main(){
    test_malloc();
    test_free_one_block();
    test_free_two_blocks();
    test_expand_heap();
    test_new_region_in_another_place();
}